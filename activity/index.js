console.log('Activity 18');
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: [
        'Pikachu',
        'Snorlax',
        'Bulbasaur',
        'Muk',
        ],
	friends: {
        hoenn: ['May', 'Max'],
        kanto: ['Brock', 'Misty'] 
    }
}
console.log(trainer);
// adding trainer object method named talk that prints out message
trainer.talk = function() {
	console.log('Pikachu! I choose you!');
}

// acces traner object using dot and square notation
console.warn('Result of dot notation');
console.log(trainer.name);//dot notation
console.warn('Result of bracket notation');
console.log(trainer['pokemon']);


// invoke/calling the trainer object method
console.warn('Result of talk method');
trainer.talk()

// Create a constructor for creating a pokemon with the following properties
function Pokemon(name, level, health, attack){
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = 2 * level;

    // tackle
    this.tackle = function (target) { 
      let hp = this.attack - target.health
      console.log(`${this.name} tackled ${target.name} `);
      console.log(`${this.name} health is now reduce to ${hp}`);
      console.log(target);
      if (hp <= 0) {
        console.log(this.faint());
      } 

    }

    // faint
    this.faint = function () {
		return `pokemon has fainted`
    }
}

// Create/instantiate several pokemon object from the constructor with varying name and level properties.
let totodile = new Pokemon('Totodile', 20,);
console.log(totodile);
let taillow = new Pokemon('Taillow', 16,);
console.log(taillow);
let torkoal = new Pokemon('Torkoal', 10,);
console.log(torkoal);

totodile.tackle(taillow);
taillow.tackle(totodile);

